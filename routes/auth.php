<?php
/**
 * Handle only auth user access
 * 
 * @author Md. Moktar Ali
 */
use Illuminate\Support\Facades\Route;

Route::group(['middleware'  =>  'auth:api'], function () {
    Route::group(['prefix'=>'/auth'], function() {
        Route::get('/user-roles/{userId}', 'AuthController@userRoles');
        Route::get('/components-by-role/{roleId}', 'AuthController@componentsByRole');
    });
});