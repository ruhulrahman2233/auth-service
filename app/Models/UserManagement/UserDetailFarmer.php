<?php

namespace App\Models\UserManagement;

use Illuminate\Database\Eloquent\Model;

class UserDetailFarmer extends Model
{
    protected $table = "user_detail_farmers";

    protected $fillable = [
        'user_id', 'email', 'mobile_no', 'name', 'name_bn', 'service_type', 'service_type_bn', 'division_id', 'region_id', 'district_id', 'upazilla_id', 'union_id', 'warehouse_type_id', 'warehouse_id', 'address', 'address_bn', 'father_name', 'father_name_bn', 'land_type_id', 'land_area', 'remarks', 'remarks_bn', 'nid','save_status'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
