<?php

namespace App\Models\UserManagement;

use Illuminate\Database\Eloquent\Model;

class SecretQuestion extends Model
{
    protected $table ="secret_questions";

    protected $fillable = [
        'question_name','question_name_bn'
    ];
}
