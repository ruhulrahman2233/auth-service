<?php

namespace App\Models\UserManagement;

use Illuminate\Database\Eloquent\Model;

class SecretQuestionAnswer extends Model
{
    protected $table = "secret_question_answers";
}
