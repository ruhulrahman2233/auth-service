<?php
namespace App\Http\Validations\UserManagement;

use Validator;

class WarehouseUserValidation
{
    /**
     * WareHouse User Validation
     */
    public static function validate ($request ,$id = 0)
    {
        $validator = Validator::make($request->all(), [
            'mobile_no'             => 'required|unique:user_detail_farmers,mobile_no,'.$id,
            'name'                  => 'required',
            'region_id'             =>'required',
            'district_id'           =>'required',
            'upazilla_id'           =>'required',
            'warehouse_type_id'     =>'required',
            'warehouse_id'          =>'required',
            'address'               =>'required',
            // 'father_name'           =>'required',
            'land_type_id'          =>'required',
            'land_area'             =>'required',
            'password'              => 'required|min:6',
            'username'              =>'sometimes:nullable|unique:users,username,' .$id
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];

    }
}
