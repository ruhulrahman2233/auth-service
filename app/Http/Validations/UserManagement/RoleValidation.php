<?php
namespace app\Http\Validations\UserManagement;

use Validator;

class RoleValidation
{
    /**
     * User Role Valiation
     */
    public static function validate($request, $id=0)
    {
        $validator = Validator::make($request->all(), [
            'role_name'       => 'required|unique:roles,role_name,'.$id,
            'role_name_bn'    => 'required',
        ]);

        if ($validator->fails()) {
            return([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }
        return ['success'=>true];

    }
}