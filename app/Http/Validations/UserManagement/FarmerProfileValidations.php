<?php

namespace App\Http\Validations\UserManagement;

use Validator;

class FarmerProfileValidations
{
    /**
     * Farmer Profile Validation
    */
    public static function validate($request, $id = 0)
    {
        $validator = Validator::make($request->all(), [
//            'division_id' => 'required|integer',
//            'district_id' => 'required|integer',
//            'upazilla_id' => 'required|integer',
//            'union_id' => 'required|integer',
//            'warehouse_type_id' => 'required|integer|exists:master_warehouse_types,id',
//            'warehouse_id' => 'required|integer|exists:master_warehouse_infos,id',
//            'region_id' => 'required|integer|exists:master_regions,id',
            'name' => 'required|string',
            'name_bn' => 'required|string',
            'father_name' => 'required|string',
            'father_name_bn' => 'nullable|string',
            'mobile_no' => 'required|string',
            'address' => 'required|string',
            'address_bn' => 'nullable|string',
            // 'land_type_id' => 'required|integer',
            'land_area' => 'required|numeric',
            // 'service_type' => 'required|string',
            // 'service_type_bn' => 'nullable|string',
            'nid' => 'nullable|string',
            'password' => 'nullable|confirmed'
        ]);

        if ($validator->fails()) {
            return([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }

        return ['success'=>true];
    }
}
