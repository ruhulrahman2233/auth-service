<?php
namespace app\Http\Validations\UserManagement;

use Validator;

class UserWarehouseValidation
{
    /**
     * user account validation
     */
    public static function validate($request, $id =0)
    {
        $validator = Validator::make($request->all(), [
            'username'     => 'required|unique:users,username,'.$id,
            'phone_no' => 'required|unique:users,mobile_no,'.$id,
            'email'     => 'required|unique:users,email,'.$id,
            'name'      => 'required',
            'password'  => 'required|min:6',
            'repeat_password'  => 'required|min:6|same:password',
            'org_id' => 'required',
            'office_id' => 'required',
            'office_type_id' => 'required',
            'designation_id' => 'required',
            'warehouse_id' => 'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }

        return ['success'=> 'true'];
    }
}
