<?php
namespace app\Http\Validations\UserManagement;

use Validator;

class SecretQuestionValidation
{
    /**
     * user secret validation
     */
    public static function validate($request, $id =0)
    {
        $validator = Validator::make($request->all(), [
            'question_name'       => 'required|unique:secret_questions,question_name,'.$id,
            'question_name_bn'    => 'required',
        ]);

        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors'  => $validator->errors()
            ]);
        }
        
        return ['success'=> 'true'];
    }
}