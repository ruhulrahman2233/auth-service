<?php

namespace App\Http\Controllers\UserTaskManagement;

use App\Http\Controllers\Controller;
use DB;
use Laravel\Lumen\Http\Request;

class TaskManagementController extends Controller
{
    public function getUserList ($id, $userTypeId, $officeId, $upazillaId) {
//        return [$id, $userTypeId, $officeId, $upazillaId];
        $type = null;
        if (intval($id) == 1) {
            $type = $id;
        }
        if (intval($id) == 2) {
            $type = 0; // 0 = internal users, 1 = irrigation farmer, 2 = warehouse farmer
        }
        if ($type !== null) {
            $query = DB::table('users')
                ->join('user_details','users.id', '=','user_details.user_id')
                ->where('users.user_type_id', $type)
                ->where('users.status', 0)
                ->select('users.username', 'users.user_type_id', 'users.id', 'user_details.user_id', 'user_details.office_id');

            if ($officeId && $userTypeId != 0) { // 0 = super admin, so we are checking if the user is not super admin
                $query->where('user_details.office_id', $officeId);
            }

            $data = $query->get();

            return response([
                'success' => true,
                'message' => 'Task Assign User List',
                'data' => $data
            ]);
        } else {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }
    }
}
