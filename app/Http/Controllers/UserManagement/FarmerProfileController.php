<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use App\Http\Validations\UserManagement\FarmerProfileValidations;
use App\Models\UserManagement\UserDetailFarmer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;

class FarmerProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $userDetailFarmer = UserDetailFarmer::whereUserId(user_id())->first();

        if ($request->isMethod('PUT')) {

            $validationResult = FarmerProfileValidations::validate($request);

            if (!$validationResult['success']) {
                return response($validationResult);
            }

            DB::beginTransaction();

            try {
                $userDetailFarmer->fill($request->all());
                $userDetailFarmer->save();

                if($request->filled('password')) {
                    $userDetailFarmer->user()->update([
                        'password' => Hash::make($request->password)
                    ]);
                }

                DB::commit();

                return response([
                    'success' => true,
                    'message' => 'Data save successfully',
                    'data'    => $userDetailFarmer
                ]);
            } catch (\Exception $ex) {
                DB::rollBack();

                return response([
                    'success' => false,
                    'message' => 'Failed to save data.',
                    'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
                ]);
            }
        } elseif ($request->isMethod('GET')) {

            if ($userDetailFarmer) {
                return response([
                    'success' => true,
                    'message' => 'User found!',
                    'data'    => $userDetailFarmer
                ]);
            }

            return response([
                'success' => false,
                'message' => 'User nor found',
                'data'    => ''
            ]);
        }
    }
}
