<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserManagement\UserDetailFarmer;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use DB;

class RegistrationApproveController extends Controller
{
    /**
     * Registration Reject Status
     */
    public function approveStatus($id)
    {
        $regApprove = UserDetailFarmer::find($id);

        if (!$regApprove) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        if (request('comments')) {
            $regApprove->comments = request('comments');
        } else {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $regApprove->status = 1;
        $regApprove->comments_bn = request('comments_bn');
        $regApprove->update();

        save_log([
            'data_id' => $regApprove->id,
            'table_name' => 'user_detail_farmers',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $regApprove
        ]);

    }

     /**
     * Registration Approve Status
     */
    public function RejectStatus($id)
    {
        $regReject = UserDetailFarmer::find($id);

        if (!$regReject) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $regReject->status = 4;
        $regReject->comments = request('comments');
        $regReject->comments_bn = request('comments_bn');
        $regReject->update();

        save_log([
            'data_id' => $regReject->id,
            'table_name' => 'user_detail_farmers',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $regReject
        ]);

    }

    /**
     * Registration Recommendation Data
     */
    public function regRecommendationData(Request $request)
    {
        // $query = DB::table('users')                        
        // ->join('user_detail_farmers','users.id', '=','user_detail_farmers.user_id')
        // ->select('users.*',
        //             'user_detail_farmers.district_id','user_detail_farmers.upazilla_id',
        //             'user_detail_farmers.union_id','user_detail_farmers.warehouse_id',
        //             'user_detail_farmers.name','user_detail_farmers.name_bn',
        //             'user_detail_farmers.father_name','user_detail_farmers.father_name_bn',
        //             'user_detail_farmers.address','user_detail_farmers.address_bn',
        //             'user_detail_farmers.land_area')->where('users.status', 3);

        $query = UserDetailFarmer::select('*')->where('status', 3);

        if ($request->has('warehouse_id')) {
            $query->where('warehouse_id', $request->warehouse_id);
        }

        $list = $query->paginate($request->per_page ?? 10);

        return response()->json([
            'success'=> true,
            'message' =>'recommendation data list',
            'data' =>$list
        ]);

    }
}
