<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Validations\UserManagement\WarehouseUserValidation;
use App\Models\UserManagement\UserDetailFarmer;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use DB;

class WarehouseUserController extends Controller
{
    /**
     * warehouse user registration store
     */
    public function warehouseregister(Request $request)
    {
        $validationResult = WarehouseUserValidation::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        DB::beginTransaction();

        try {

            $user                = new User();
            $user->name          = $request->name;
            $user->username      = $request->mobile_no;
            $user->email         = "fm_".$request->mobile_no."@gmail.com";
            $user->user_type_id  = 2;
            $user->status        = 2;
            $user->password      = Hash::make($request->password);
            $user->save();

            save_log([
                'data_id'    => $user->id,
                'table_name' => 'users'
            ]);

            $userDetailFarmer                      = new UserDetailFarmer();
            $userDetailFarmer->user_id             = $user->id;
            $userDetailFarmer->email               = "fm_".$request->mobile_no."@gmail.com";
            $userDetailFarmer->mobile_no           = $request->mobile_no;
            $userDetailFarmer->name                = $request->name;
            $userDetailFarmer->name_bn             = $request->name_bn;
            $userDetailFarmer->service_type        = $request->service_type;
            $userDetailFarmer->service_type_bn     = $request->service_type_bn;
            $userDetailFarmer->division_id         = (int)$request->division_id;
            $userDetailFarmer->region_id           = (int)$request->region_id;
            $userDetailFarmer->district_id         = (int)$request->district_id;
            $userDetailFarmer->upazilla_id         = (int)$request->upazilla_id;
            $userDetailFarmer->union_id            = (int)$request->union_id;
            $userDetailFarmer->warehouse_type_id   = (int)$request->warehouse_type_id;
            $userDetailFarmer->warehouse_id        = (int)$request->warehouse_id;
            $userDetailFarmer->address             = $request->address;
            $userDetailFarmer->address_bn          = $request->address_bn;
            $userDetailFarmer->father_name         = $request->father_name;
            $userDetailFarmer->father_name_bn      = $request->father_name_bn;
            $userDetailFarmer->land_type_id        = (int)$request->land_type_id;
            $userDetailFarmer->land_area           = $request->land_area;
            $userDetailFarmer->remarks             = $request->remarks;
            $userDetailFarmer->remarks_bn          = $request->remarks_bn;
            $userDetailFarmer->nid	               = $request->nid;
            $userDetailFarmer->save();

            save_log([
                'data_id'    => $userDetailFarmer->id,
                'table_name' => 'user_detail_farmers'
            ]);

            DB::commit();

        } catch (\Exception $ex) {

            DB::rollback();

            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }
        return response([
            'success' => true,
            'message' => 'Data save successfully',
            'data'    => $userDetailFarmer
        ]);
    }

}
