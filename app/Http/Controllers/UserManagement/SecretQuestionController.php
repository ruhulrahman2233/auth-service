<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserManagement\SecretQuestion;
use App\Http\Validations\UserManagement\SecretQuestionValidation;
use Validator;
use Illuminate\Support\Facades\DB;
class SecretQuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * get all secert question
     */
    public function index(Request $request)
    {
        $query = DB::table('secret_questions');
       
        if ($request->question_name) {
            $query = $query->where('question_name', 'like', "{$request->question_name}%")
                           ->orWhere('question_name_bn', 'like', "{$request->question_name}%");            
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }
        $secret = $query->orderBy('question_name', 'ASC')->paginate(request('per_page', config('app.per_page')));
        return response([
            'success' => true,
            'message' => 'Secret Question list',
            'data' => $secret
        ]);
    }

    /**
     * Secret store
     */
    public function store(Request $request)
    {
        $validationResult = SecretQuestionValidation::validate($request);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        try {
            $secret = new SecretQuestion();
            $secret->question_name     = $request->question_name;
            $secret->question_name_bn  = $request->question_name_bn;
            $secret->created_by        = (int)user_id();
            $secret->updated_by        = (int)user_id();
            $secret->save();

            save_log([
                'data_id'    => $secret->id,
                'table_name' => 'secret_questions'
            ]);
          
        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to save data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Secret data save successfully',
            'data'    => $secret,
        ]);
    }

    /**
     * secret update
     */
    public function update(Request $request, $id)
    {   
        $validationResult = SecretQuestionValidation::validate($request, $id);

        if (!$validationResult['success']) {
            return response($validationResult);
        }

        $secret = SecretQuestion::find($id);

        if (!$secret) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        try {
            $secret->question_name     = $request->question_name;
            $secret->question_name_bn  = $request->question_name_bn;
            $secret->updated_by        = (int)user_id();
            $secret->save();

            save_log([
                'data_id'       => $secret->id,
                'table_name'    => 'secret_questions',
                'execution_type'=> 1
            ]);

        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Failed to update data.',
                'errors'  => env('APP_ENV') !== 'production' ? $ex->getMessage() : ""
            ]);
        }

        return response([
            'success' => true,
            'message' => 'Module Data update successfully',
            'data'    => $secret
        ]);
    }

    /**
     * secret status update
     */
    public function toggleStatus($id)
    {
        $secret = SecretQuestion::find($id);

        if (!$secret) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $secret->status = $secret->status ? 0 : 1;
        $secret->save();

        save_log([
            'data_id'       => $secret->id,
            'table_name'    => 'secret_questions',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Secret Data updated successfully',
            'data'    => $secret
        ]);
    }

    /**
     * secret destroy
     */
    public function destroy($id)
    {
        $secret = SecretQuestion::find($id);

        if (!$secret) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $secret->delete();

        save_log([
            'data_id'       => $id,
            'table_name'    => 'secret_questions',
            'execution_type'=> 2
        ]);

        return response([
            'success' => true,
            'message' => 'Secret Data deleted successfully'
        ]);
    }
    
    public function secretQuestionDropdown() {
        $list = SecretQuestion::where('status', 0)->select('id as value', 'question_name as text_en', 'question_name_bn as text_bn')->get();
        return response([
            'success' => true,
            'data' => $list,
            'message' => 'Secret question list'
        ]);
    }
}
