<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserManagement\UserDetailFarmer;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use DB;

class RegistrationRecommendationController extends Controller
{
      /**
     * Warehouse change Status list
     */
    public function changeStatus($id)
    {
        $regRecommendation = UserDetailFarmer::find($id);

        if (!$regRecommendation) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        if (request('comments')) {
            $regRecommendation->comments = request('comments');
        } else {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        $regRecommendation->status = 3;
        $regRecommendation->comments_bn = request('comments_bn');
        $regRecommendation->update();

        save_log([
            'data_id' => $regRecommendation->id,
            'table_name' => 'user_detail_farmers',
            'execution_type' => 2,
        ]);

        return response([
            'success' => true,
            'message' => 'Data updated successfully',
            'data'    => $regRecommendation
        ]);

    }

    /**
     * WareHouse Recommendation list
     */
    public function regRecommendationList(Request $request)
    {
        // $query = DB::table('users')                        
        //     ->join('user_detail_farmers','users.id', '=','user_detail_farmers.user_id')
        //     ->select('users.*',
        //                 'user_detail_farmers.district_id','user_detail_farmers.upazilla_id',
        //                 'user_detail_farmers.union_id','user_detail_farmers.warehouse_id',
        //                 'user_detail_farmers.name','user_detail_farmers.name_bn',
        //                 'user_detail_farmers.father_name','user_detail_farmers.father_name_bn',
        //                 'user_detail_farmers.address','user_detail_farmers.address_bn',
        //                 'user_detail_farmers.land_area')->where('users.status', 2);
        $query = UserDetailFarmer::select('*')->where('status', 2);

        if ($request->has('warehouse_id')) {
            $query->where('warehouse_id', $request->warehouse_id);
        } 

        $list = $query->paginate($request->per_page ?? 10);

        return response()->json([
            'success'=> true,
            'message' =>'recommendation data list',
            'data' =>$list
        ]);

                     
    }
    
    /**
     * Recommendation Details
     */
    public function getDetails($user_id)
    { 
        // $list = DB::table('users')                        
        //             ->join('user_detail_farmers','users.id', '=','user_detail_farmers.user_id')
        //             ->select('users.*','user_detail_farmers.*')
        //                     ->where('users.status', 2)
        //                     ->where('users.id', $user_id)
        //                     ->first();
        $list = UserDetailFarmer::select('*')
                ->where('id',$user_id)
                ->where('status', 2)
                ->first();

        return response()->json([
            'success'   => true,
            'message'   => 'Recommendation details',
            'data'      => $list
        ]);
    }

     /**
     *  Details List
     */
    public function getRecommendationDetails($user_id)
    { 
        $list = UserDetailFarmer::select('*')
                ->where('id',$user_id)
                ->where('status', 3)
                ->first();                    

        return response()->json([
            'success'   => true,
            'message'   => 'Recommendation details List',
            'data'      => $list
        ]);
    }
}
