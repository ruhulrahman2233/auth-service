<?php
/**
 * Handle only Access Permission related task
 * @author Md. Moktar Ali
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserManagement\MenuWiseRole;
use App\Library\CommonInfo;
use DB;

class AuthController extends Controller
{
    /**
     * Get all roles of a user
     */
    public function userRoles(Request $request, $userId)
    {
        $userRoles = User::find($userId)->roles()->get();

        return response([
            'success' => true,
            'message' => 'User roles',
            'data' => $userRoles
        ]);
    }

    /**
     * Retrieves all component ids of all menus which are assigned to a role.
     */
    public function componentsByRole($roleId)
    {
        try {
            $query = MenuWiseRole::select('component_id');
    
            if ($roleId != 1) {
                $query = $query->where('role_id', $roleId);
            }
    
            $componentIds = $query->groupBy('component_id')->pluck('component_id')->all();
            
            if (count($componentIds) === 0) {
                return response([
                    'success' => false,
                    'message' => 'This role has not been assigned any menu',
                    'message_i18n_code' => 'authentication.noPrivilege'
                ]);   
            }
    
            $components = CommonInfo::getAllComponents();
            $components = json_decode($components, true);
    
            if (!isset($components['success'])) {
                return response([
                    'success' => false,
                    'message' => 'Failed get components.',
                    'message_i18n_code' => 'authentication.noComponent'
                ]);
            }
            
            $selectedComponents = [];
    
            foreach ($components['data'] as $component) {
                if (in_array($component['id'], $componentIds)) {
                    $selectedComponents[] = $component;
                }
            }
        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'message' => 'Faield to get components',
                'message_i18n_code' => 'authentication.serverError'
            ]);
        }

        return response([
            'success' => true,
            'data' => $selectedComponents
        ]);
    }
}
