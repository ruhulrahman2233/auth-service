<?php

namespace App\Library;

use Illuminate\Support\Facades\Cache;

class CommonInfo
{
    public static function farmerUserDetail () {
        try {
            $data = \App\Models\User::with(['userDetail', 'userDetailFarmer'])->find(user_id());
        } catch (\Exception $ex) {
            return response([
                'success' => false,
                'data' => null,
                'username' => null
            ]);
        }

        return response([
            'success' => true,
            'data' => $data,
            'username' => username()
        ]);
    }


    public static function getAuthUserDetail($request)
    {
        $officeDetail = null;

        if ($request->user()->user_type_id === 0) {
            $officeDetail = self::getOrgAdminUserOfficeDetail($request->user()->userDetail->office_id);
            $officeDetail = json_decode($officeDetail);
            $officeDetail = (isset($officeDetail->success) && $officeDetail->success) ? $officeDetail->data : null;
        }
        return response([
            'success' => true,
            'data' => $request->user()->user_type_id ? $request->user() : $request->user()->userDetail,
            'username' => $request->user()->username,
            'user_id' => $request->user()->id,
            'user_type' => $request->user()->user_type_id,
            'is_org_admin' => $request->user()->is_org_admin,
            'office_detail' => $officeDetail
        ]);
    }

    public static function getOrgAdminUserOfficeDetail($officeId)
    {
        $baseUrl = config('app.base_url.common_service');
        $uri = "/auth-user-office-detail/{$officeId}";
        $param = [];
        return \App\Library\RestService::getData($baseUrl, $uri, $param);
    }

    /**
     * All Components are cached for 24 hours. Which are retrieved from 
     */
    public static function getAllComponents()
    {
        $value = Cache::remember('allComponents', 86400, function () {
            $baseUrl = config('app.base_url.common_service');
            $uri = "/common/component-list";
            $param = [];
            return \App\Library\RestService::getData($baseUrl, $uri, $param);
        });
         
        return $value;
    }

}
