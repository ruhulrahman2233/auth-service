<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('supervisor_id')->nullable();
            $table->string('name',100);
            $table->string('email',100);
            $table->string('name_bn')->nullable();
            $table->unsignedBigInteger('org_id');
            $table->unsignedBigInteger('office_id');
            $table->integer('warehouse_id')->nullable();
            $table->string('phone_no', 100)->nullable();           
            $table->unsignedBigInteger('office_type_id')->nullable();             
            $table->unsignedBigInteger('role_id')->nullable(); 
            $table->string('photo')->nullable();            
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->integer('status')->default(0)->comment('0=active, 1=inactive');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('supervisor_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
