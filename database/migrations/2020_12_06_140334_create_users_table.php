<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('name_bn')->default('অজানা নাম');
            $table->string('username')->unique()->index("u_index");;
            $table->string('email')->unique()->index("email_index");
            $table->string('is_org_admin')->default(0)->comment('0 = normal users, 1 = org admin');
            $table->string('password');           
            $table->integer('user_type_id')->default(0)->comment('0 = internal users, 1 = irrigation farmer, 2= warehouse farmer, 3 = Ginner , 4 =Grower,5=Divisional Head , 6 = dealer, 7=grant');
            $table->timestamp('last_logged_in')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
