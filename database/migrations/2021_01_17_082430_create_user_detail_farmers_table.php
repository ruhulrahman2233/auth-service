<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailFarmersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_detail_farmers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('email',100);
            $table->string('mobile_no',100);
            $table->string('name',100);           
            $table->string('name_bn')->nullable();
            $table->string('service_type')->nullable();
            $table->string('service_type_bn')->nullable();
            $table->unsignedBigInteger('division_id')->nullable();
            $table->unsignedBigInteger('region_id')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->unsignedBigInteger('upazilla_id')->nullable();
            $table->unsignedBigInteger('union_id')->nullable();
            $table->unsignedBigInteger('warehouse_type_id')->nullable();
            $table->unsignedBigInteger('warehouse_id')->nullable();
            $table->string('nid', 100)->nullable();
            $table->string('address')->nullable();
            $table->string('address_bn')->nullable();
            $table->string('father_name',100)->nullable();           
            $table->string('father_name_bn')->nullable();
            $table->tinyInteger('land_type_id')->nullable();
            $table->float('land_area')->nullable();
            $table->string('remarks')->nullable();
            $table->string('remarks_bn')->nullable();
            $table->integer('save_status')->default(1)->comment('1 = draft, 2 = permanent');
            $table->integer('status')->default(0)->comment('1 = approved, 3 = recommended, 4 = rejected');
            $table->text('comments')->nullable();
            $table->text('comments_bn')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_detail_farmers');
    }
}
